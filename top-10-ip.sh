#!/bin/bash
set -e
ips=()
if [ -f "$1" ]; then
  for ip in `(awk '{print $1}' "$1" | uniq)`
    do
      ips+=( "bytes: $(cat "$1" | grep "$ip" | awk '{s+=$(10)} END {print s}') IP: $ip" )
    done
  IFS=$'\n' sorted=($(sort -nr -k2 <<< "${ips[*]}")); unset IFS
  for ((i=0; i<"${#sorted[@]}" && i<10; i++))
    do
      echo "${sorted[i]}"
    done
else
  echo 'File not found'
fi