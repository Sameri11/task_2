#!/bin/bash
set -e
if [ -f "$1" ]; then
  echo $(awk '{print $1}' "$1" | sort | uniq -c | sort -nr | (head -n1) | awk '{print $2}')
else
  echo "File not found"
fi