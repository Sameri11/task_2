## Instructions

### Most frequent IP

To show most frequent IP use:

<code>$ bash most-frequent.sh \<filepath></code>

### TOP 10 of IPs

To show top 10 IPs by traffic consumption use:

<code>$ bash top-10-ip.sh \<filepath></code>
